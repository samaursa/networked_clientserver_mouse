#version 330 core

// We need one out (used to be g_FragColor)
in  vec2 v_texCoord;
out vec3 o_color;

uniform vec4 u_col;

void main()
{
	o_color = u_col.rgb;
}
