#include "Packet.h"

#include "RakNetTypes.h"

namespace f_packet
{
  MousePos CreatePacket(RakNet::RakNetGUID a_guid)
  {
    MousePos mp;
    mp.m_guid = a_guid;
    mp.m_x = 0;
    mp.m_y = 0;

    return mp;
  }

  unsigned char GetPacketID(RakNet::Packet* p)
  {
    // we want the first byte
    return p->data[0];
  }
};