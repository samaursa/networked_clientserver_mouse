#include <tlocCore/tloc_core.h>
#include "Packet.h"
#include "PacketHandler.h"

#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"

PacketHandler_I::
PacketHandler_I(rak_peer_ptr a_peer)
: m_peer(a_peer)
{ TLOC_ASSERT_NOT_NULL(a_peer); }

void
PacketHandler_I::
Update(sec_type a_deltaT)
{
  HandlePackets(a_deltaT, m_peer);
}

void
PacketHandler_I::
Pre_HandlePackets(sec_type , rak_peer_ptr )
{ }

void
PacketHandler_I::
HandlePackets(sec_type a_deltaT, rak_peer_ptr a_peer)
{
  Pre_HandlePackets(a_deltaT, a_peer);
  DoHandlePackets(a_deltaT, a_peer);
  Post_HandlePackets(a_deltaT, a_peer);
}

void
PacketHandler_I::
Post_HandlePackets(sec_type , rak_peer_ptr )
{
}

void
PacketHandler_I::
DoHandlePackets(sec_type a_deltaT, rak_peer_ptr a_peer)
{
  for (RakNet::Packet* p = a_peer->Receive();
       p;
       a_peer->DeallocatePacket(p), p = a_peer->Receive())
  {
    auto packetID = f_packet::GetPacketID(p);

    // if derived classes have 
    if (HandleMessage(packetID, a_peer, p)) { continue; }

    switch (packetID)
    {
      // handle common messages
    case ID_CONNECTION_LOST:
    { printf("\nConnection lost from %s", p->systemAddress.ToString(true)); break; }

    default:
    { printf("\nReceived unhandled packet"); } }
  }

  Post_HandlePackets(a_deltaT, a_peer);
}

#include <tlocCore/smart_ptr/tloc_smart_ptr.inl.h>
TLOC_EXPLICITLY_INSTANTIATE_ALL_SMART_PTRS(PacketHandler_I);