#include "PlayerInfo.h"

#include <tlocCore/utilities/tlocType.h>

PlayerInfo::
PlayerInfo()
: m_state(f_packet::CreatePacket(RakNet::UNASSIGNED_RAKNET_GUID))
{ }

PlayerInfo::
PlayerInfo(MousePos a_mousePos)
: m_state(a_mousePos)
{ }

auto
PlayerInfo::
SetGuid(const guid_type& a_guid) -> this_type&
{
  m_state.m_guid = a_guid;
  return *this;
}

PlayerInfoPair::
PlayerInfoPair()
: m_totalTimeToInter(1.0/10.0)
, m_currTime(0)
{ }

PlayerInfoPair::
PlayerInfoPair(player_info a_starting)
: PlayerInfoPair()
{ 
  m_starting = a_starting;
  m_current = m_starting;
}

auto
PlayerInfoPair::
GetNext(sec_type a_deltaT) const
-> player_info
{
  if (m_targets.size() == 0)
  { return m_starting; }

  m_currTime += a_deltaT;

  if (m_currTime >= m_totalTimeToInter) 
  { 
    m_currTime = 0.0;
    m_starting = m_targets.front();
    m_current = m_starting;
    m_targets.erase(m_targets.begin());

    return m_current;
  }

  const auto t = m_currTime / m_totalTimeToInter;

  auto startState = m_starting.GetState();
  auto targetState = m_targets.front().GetState();

  auto xVal = startState.m_x * (1-t) + targetState.m_x * t;
  auto yVal = startState.m_y * (1-t) + targetState.m_y * t;

  MousePos mp;
  mp.m_x = tl_core_utils::CastNumber<int>(xVal);
  mp.m_y = tl_core_utils::CastNumber<int>(yVal);
  mp.m_guid = startState.m_guid;

  m_current.SetState(mp);

  return PlayerInfo(mp);
}

void
PlayerInfoPair::
AddTarget(player_info a_target)
{
  m_targets.push_back(a_target);
}

auto
PlayerInfoPair::
SetStartingInfo(const player_info& a_pi) 
-> this_type&
{
  m_currTime = 0.0;
  m_starting = a_pi;
  m_current = m_starting;
  return *this;
}