#pragma once
#include <tlocCore/utilities/tlocUtils.h>
#include <tlocCore/utilities/tlocContainerUtils.h>
#include <tlocCore/containers/tlocArray.h>
#include "Packet.h"
#include "RakNetTypes.h"

struct PlayerInfo
{
  typedef PlayerInfo this_type;
  typedef RakNet::RakNetGUID              guid_type;

  PlayerInfo();
  PlayerInfo(MousePos a_mousePos);

  TLOC_DECL_AND_DEF_GETTER(guid_type, GetGuid, m_state.m_guid);

private:
  MousePos m_state;

public:
  TLOC_DECL_SETTER_CHAIN(guid_type, SetGuid);
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetState, m_state);
  TLOC_DECL_AND_DEF_SETTER_CHAIN_AUTO(SetState, m_state);
};

class PlayerInfoPair
{
public:
  typedef PlayerInfoPair                      this_type;
  typedef PlayerInfo                          player_info;
  typedef tl_core_conts::Array<player_info>   player_info_cont;
  typedef double                              sec_type;

  PlayerInfoPair();
  PlayerInfoPair(player_info a_starting);

  player_info   GetNext(sec_type a_deltaT) const;
  void          AddTarget(player_info a_target);

private:
  sec_type          m_totalTimeToInter;

  mutable player_info       m_starting;
  mutable player_info       m_current;
  mutable player_info_cont  m_targets;
  mutable sec_type          m_currTime;

public:
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetStartingInfo, m_starting);
  TLOC_DECL_AND_DEF_GETTER_AUTO(GetCurrentInfo, m_current);

  TLOC_DECL_AND_DEF_SETTER_CHAIN_AUTO(SetTotalTimeToInterpolate, m_totalTimeToInter);

  TLOC_DECL_SETTER_CHAIN(player_info, SetStartingInfo);

  TLOC_DECL_AND_DEF_CONTAINER_ALL_METHODS(_targets, m_targets);
};