#include "Server.h"
#include "RakPeerInterface.h"

TLOC_DEFINE_THIS_FILE_NAME();

Server::
Server(port_type a_port)
: m_port(a_port)
{ 
  m_peer = RakNet::RakPeerInterface::GetInstance();
}

Server::
~Server()
{
  m_peer->Shutdown(300);
  RakNet::RakPeerInterface::DestroyInstance(m_peer);
}

auto
Server::
Launch() const -> error_type
{
  m_peer->SetIncomingPassword(0, 0);

  // IPV4 socket
  RakNet::SocketDescriptor  sd;
  sd.port = m_port;
  sd.socketFamily = AF_INET;

  if (m_peer->Startup(4, &sd, 1) != RakNet::RAKNET_STARTED)
  {
    printf("\nFailed to start server with IPV4 ports");
    return ErrorFailure;
  }

  m_peer->SetOccasionalPing(true);
  m_peer->SetUnreliableTimeout(1000);
  m_peer->SetMaximumIncomingConnections(4);

  TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << "SERVER IP addresses:";
  for (unsigned int i = 0; i < m_peer->GetNumberOfAddresses(); i++)
  {
    RakNet::SystemAddress sa = 
      m_peer->GetInternalID(RakNet::UNASSIGNED_SYSTEM_ADDRESS, i);
    TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() << 
      tl_core_str::Format("%i. %s (LAN=%i)", i + 1, sa.ToString(false), sa.IsLANAddress());
  }

  return ErrorSuccess;
}

#include <tlocCore/tloc_core.inl.h>
TLOC_EXPLICITLY_INSTANTIATE_ALL_SMART_PTRS(Server);