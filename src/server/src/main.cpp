#include <tlocCore/tloc_core.h>
#include <tlocGraphics/tloc_graphics.h>
#include <tlocMath/tloc_math.h>
#include <tlocPrefab/tloc_prefab.h>
#include <tlocApplication/tloc_application.h>

#include <gameAssetsPath.h>

#include "../../common/src/Server.h"
#include "../../common/src/Packet.h"
#include "../../common/src/PlayerInfo.h"
#include "../../common/src/GamePacketHandler.h"

#include "RakPeerInterface.h"

TLOC_DEFINE_THIS_FILE_NAME();

using namespace tloc;

namespace {

  core_str::String shaderPathVS("/shaders/tlocOneTextureVS.glsl");
  core_str::String shaderPathFS("/shaders/tlocOneTextureFS.glsl");

  const core_str::String g_assetsPath(GetAssetsPath());

};

// ///////////////////////////////////////////////////////////////////////
// Server Packet Handler

class PacketHandlerServer
  : public GamePacketHandler_I
{
public:
  PacketHandlerServer(rak_peer_ptr a_peer)
    : GamePacketHandler_I(a_peer)
  { }

  bool
    HandleMessage(packet_id_type a_id, rak_peer_ptr a_peer, packet_ptr a_packet)
  {
    switch(a_id)
    {
      case ID_NEW_INCOMING_CONNECTION:
      {
        TLOC_LOG_DEFAULT_INFO_FILENAME_ONLY() <<
          core_str::Format("\nNew incoming connection from %s with GUID %s",
          a_packet->systemAddress.ToString(true), a_packet->guid.ToString());

        // send the new player all the other player IDs
        core::for_each(begin_playerInfo(), end_playerInfo(),
                       [a_peer, a_packet](const PlayerInfoPair& a_pi)
        {
          MousePos mp = a_pi.GetStartingInfo().GetState();
          mp.m_packetID = packet_id::ID_NEW_PLAYER;

          a_peer->Send((const char*)&mp, sizeof(MousePos), HIGH_PRIORITY, 
                       UNRELIABLE_SEQUENCED, 0, a_packet->systemAddress, false);
        });

        // do some sanity checks and then send new player info to all other
        // players
        auto itr = core::find_if(begin_playerInfo(), end_playerInfo(), 
                                 [a_packet](const PlayerInfoPair& a_pi)
        {
          if (a_packet->guid == a_pi.GetStartingInfo().GetGuid()) { return true; }
          return false;
        });

        TLOC_ASSERT(itr == end_playerInfo(), "Player already connected.");

        if (itr == end_playerInfo())
        { 
          auto pi = PlayerInfo().SetState(f_packet::CreatePacket(a_packet->guid));
          PlayerInfoPair pip(pi);
          m_playerInfo.push_back(pip);
          itr = end_playerInfo() - 1;

          MousePos mp = m_playerInfo.back().GetStartingInfo().GetState();
          mp.m_packetID = packet_id::ID_NEW_PLAYER;

          a_peer->Send((const char*)&mp, sizeof(MousePos), HIGH_PRIORITY, 
                       UNRELIABLE_SEQUENCED, 0, a_packet->systemAddress, true);
        }

        return true;
      }
      case ID_CONNECTION_LOST:
      case ID_CONNECTION_BANNED:
      case ID_DISCONNECTION_NOTIFICATION:
      {
        auto itr = core::find_if(begin_playerInfo(), end_playerInfo(), 
                                 [a_packet](const PlayerInfoPair& a_pi)
        {
          if (a_packet->guid == a_pi.GetStartingInfo().GetGuid()) { return true; }
          return false;
        });

        if (itr != end_playerInfo())
        {
          TLOC_LOG_DEFAULT_DEBUG_FILENAME_ONLY() <<
            "Player disconnected: " << itr->GetStartingInfo().GetGuid().ToString();
          m_playerInfo.erase(itr);
        }

        return true;
      }
      case packet_id::ID_PLAYER_STATE:
      {
        auto itr = core::find_if(begin_playerInfo(), end_playerInfo(), 
                                 [a_packet](const PlayerInfoPair& a_pi)
        {
          if (a_packet->guid == a_pi.GetStartingInfo().GetGuid()) { return true; }
          return false;
        });

        if (itr == end_playerInfo())
        { 
          TLOC_LOG_DEFAULT_ERR_FILENAME_ONLY() << "Receiving state from an unknown Player(" 
            << a_packet->guid.ToString() << ")";
        }
        else
        {
          MousePos mp = *(MousePos*)a_packet->data;
          mp.m_packetID = packet_id::ID_PLAYER_STATE;
          itr->SetStartingInfo(mp);

          // broadcast the state to everybody EXCEPT the client who sent us the
          // state
          a_peer->Send((const char*)&mp, sizeof(MousePos), HIGH_PRIORITY, 
                       UNRELIABLE_SEQUENCED, 0, a_packet->systemAddress, true);
        }

        return true;
      };
    }

    return false;
  }
};

#include <tlocCore/smart_ptr/tloc_smart_ptr.inl.h>
TLOC_EXPLICITLY_INSTANTIATE_ALL_SMART_PTRS(PacketHandlerServer);

// ///////////////////////////////////////////////////////////////////////
// Demo app

class Demo 
  : public Application
{
public:
  Demo()
    : Application("2LoC Engine")
  { }

private:
  error_type Post_Initialize() override
  {
    auto& scene = GetScene();
    scene->AddSystem<gfx_cs::MaterialSystem>();
    auto meshSys = scene->AddSystem<gfx_cs::MeshRenderSystem>();
    meshSys->SetRenderer(GetRenderer());

    //------------------------------------------------------------------------

    auto_cref to = app::resources::f_resource::LoadImageAsTextureObject
      (core_io::Path(g_assetsPath + "/images/engine_logo.png"));

    gfx_gl::uniform_vso  u_to;
    u_to->SetName("s_texture").SetValueAs(*to);

    //------------------------------------------------------------------------

    math_t::Rectf_c rect(math_t::Rectf_c::width(1.0f * 2.0f),
                         math_t::Rectf_c::height(GetWindow()->GetAspectRatio().Get() * 2.0f));

    core_cs::entity_vptr q = scene->CreatePrefab<pref_gfx::Quad>()
      .Dimensions(rect).Create();

    scene->CreatePrefab<pref_gfx::Material>()
      .AssetsPath(GetAssetsPath())
      .AddUniform(u_to.get())
      .Add(q, core_io::Path(shaderPathVS), core_io::Path(shaderPathFS));

    // -----------------------------------------------------------------------
    // networking

    m_server = core_sptr::MakeUnique<Server>(Server::port_type(30000));
    m_server->Launch();

    m_ph = core_sptr::MakeUnique<PacketHandlerServer>(m_server->GetPeer());
    
    return Application::Post_Initialize();
  }

  void
    Pre_Update(sec_type a_deltaT)
  {
    if (m_ph) { m_ph->Update(a_deltaT); }

    int counter = 0;
    core::for_each(m_ph->begin_playerInfo(), m_ph->end_playerInfo(), 
                   [&counter, this](const PlayerInfoPair& a_pi)
    {
        TLOC_LOG_DEFAULT_INFO_NO_FILENAME()
          << "Player" << counter++ << ": " << ": " 
          << a_pi.GetStartingInfo().GetState().m_x << ", " << a_pi.GetStartingInfo().GetState().m_y;
    });

    Application::Pre_Update(a_deltaT);
  }

private:
  server_uptr m_server;
  game_packet_handler_uptr m_ph;
};

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

int TLOC_MAIN(int , char *[])
{
  Demo demo;
  demo.Initialize(core_ds::MakeTuple(800, 800));
  demo.Run();

  //------------------------------------------------------------------------
  // Exiting
  TLOC_LOG_CORE_INFO() << "Exiting normally";

  return 0;

}
