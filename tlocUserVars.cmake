if(${SOLUTION_NAME} STREQUAL "solution")
  set(SOLUTION_NAME "solution_${SOLUTION_BUILD_FOLDER_NAME}" CACHE STRING "You may change the name based on build configurations" FORCE)
  project(${SOLUTION_NAME})
endif()

set(TLOC_DEP_INSTALL_PATH "${CMAKE_SOURCE_DIR}/3rdParty/2LoC/dep/build_2013_dist/" CACHE PATH "" FORCE)
set(TLOC_ENGINE_INSTALL_PATH "${CMAKE_SOURCE_DIR}/3rdParty/2LoC/engine/build_2013_dist/" CACHE PATH "" FORCE)

set(SOLUTION_EXECUTABLE_PROJECTS "client;")
list(APPEND SOLUTION_EXECUTABLE_PROJECTS "server;")

set(SOLUTION_LIBRARY_PROJECTS "common;")
#list(APPEND SOLUTION_LIBRARY_PROJECTS "nextLibraryProject;")

#set(SOLUTION_TEST_PROJECTS "skopworksTest;")
#list(APPEND SOLUTION_TEST_PROJECTS "nextTestProject;")
